Contributing to the Slixmpp project
===================================

To contribute, the preferred way is to commit your changes on some
publicly-available git repository (on a fork `on codeberg
<https://codeberg.org/poezio/slixmpp>`_ or on your own repository) and to
notify the developers with either:
 - a merge request `on the repository <https://codeberg.org/poezio/slixmpp/compare/master...master>`_
 - a ticket `on the bug tracker <https://codeberg.org/poezio/slixmpp/issues/new>`_
 - a simple message on `the XMPP MUC <xmpp:slixmpp@muc.poez.io?join>`_
