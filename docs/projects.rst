Projects Using Slixmpp
======================

This page enumerates software in the form of applications, bots and gateways utilizing the XMPP protocols with slixmpp.

Applications
------------

sendxmpp-py
~~~~~~~~~~~
sendxmpp is a command line program and is the XMPP equivalent of sendmail. It is a Python version of the original sendxmpp which is written in Perl.

- `Source <https://code.moparisthebest.com/moparisthebest/sendxmpp-py>`__
- `Groupchat <xmpp:xmpp-ircd@chatrooms.hackerposse.com?join>`__

Bots
----

BotLogMauve
~~~~~~~~~~~
XMPP bot which logs groupchat messages. Logs are in text format, with one file per day and per groupchat.

- `Source <https://git.khaganat.net/khaganat/BotLogMauve>`__

BukuBot
~~~~~~~
BukuBot makes it possible to manage and search your bookmarks from your chat.

- `Source <https://codeberg.org/sch/BukuBot>`__

LinkBot
~~~~~~~
This bot reveals the title of any shared link in a groupchat for quick content insight.

- `Source <https://git.xmpp-it.net/mario/XMPPBot>`__

llama-bot
~~~~~~~~~
Llama-bot enables engaging communication with the LLM (large language model) of llama.cpp, providing seamless and dynamic conversation with it.

- `Source <https://github.com/decent-im/llama-bot>`__
- `Demo <xmpp:llama@decent.im?message>`__

Morbot
~~~~~~
Morbot is a simple Slixmpp bot that will take new articles from listed RSS feeds and send them to assigned XMPP MUCs.

- `Source <https://codeberg.org/TheCoffeMaker/Morbot>`__

Slixfeed
~~~~~~~~
Slixfeed aims to be an easy to use and fully-featured news aggregator bot for XMPP. It provides a convenient access to Blogs, Fediverse and News websites along with filtering functionality.

- `Groupchat <xmpp:slixfeed@chat.woodpeckersnest.space?join>`__
- `Source <https://gitgud.io/sjehuda/slixfeed>`__

sms4you
~~~~~~~
sms4you forwards messages from and to SMS and connects either with sms4you-xmpp or sms4you-email to choose the other mean of communication. Nice for receiving or sending SMS, independently from carrying a SIM card.

- `Homepage <https://sms4you-team.pages.debian.net/sms4you/>`__
- `Source <https://salsa.debian.org/sms4you-team/sms4you>`__

Stable Diffusion
~~~~~~~~~~~~~~~~
XMPP bot that generates digital images from textual descriptions.

- `Groupchat <xmpp:slidge@conference.nicoco.fr?join>`__
- `Source <https://www.nicoco.fr/blog/2022/08/31/xmpp-bot-stable-diffusion/>`__

WhisperBot
~~~~~~~~~~
XMPP bot that transliterates audio messages using OpenAI's Whisper libraries.

- `Source <https://codeberg.org/TheCoffeMaker/WhisperBot>`__

XMPP MUC Message Gateway
~~~~~~~~~~~~~~~~~~~~~~~~
A multipurpose JSON forwarder microservice from HTTP POST to XMPP MUC room over TLSv1.2 with SliXMPP.

- `Source <https://github.com/immanuelfodor/xmpp-muc-message-gateway>`__

Services
--------

AtomToPubsub
~~~~~~~~~~~~
AtomToPubsub is a simple Python script that parses Atom + RSS feeds and pushes the entries to a designated XMPP Pubsub Node.

- `Groupchat <xmpp:movim@conference.movim.eu?join>`__
- `Source <https://github.com/imattau/atomtopubsub>`__

Slidge
~~~~~~

Slidge is a general purpose XMPP gateway framework in Python.

- `Groupchat <xmpp:slidge@conference.nicoco.fr?join>`__
- `Homepage <https://slidge.im/core/>`__
- `Source <https://sr.ht/~nicoco/slidge>`__
